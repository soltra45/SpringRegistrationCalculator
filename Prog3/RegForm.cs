﻿// Program 3
// CIS 199-02
// Due: 11/7/2019
// By: K8571

// This application calculates the earliest registration date
// and time for an undergraduate student given their class standing
// and last name.
// Decisions based on UofL Spring 2020 Priority Registration Schedule

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Prog3
{
    public partial class RegForm : Form
    {
        public RegForm()
        {
            InitializeComponent();
        }

        // Find and display earliest registration time
        private void FindRegTimeBtn_Click(object sender, EventArgs e)
        {
            const string DAY1 = "November 4"; // 1st day of registration
            const string DAY2 = "November 5"; // 2nd day of registration
            const string DAY3 = "November 6"; // 3rd day of registration
            const string DAY4 = "November 7";  // 4th day of registration
            const string DAY5 = "November 8";  // 5th day of registration
            const string DAY6 = "November 11";  // 6th day of registration

            const string TIME1 = "8:30 AM";  // 1st time block
            const string TIME2 = "10:00 AM"; // 2nd time block
            const string TIME3 = "11:30 AM"; // 3rd time block
            const string TIME4 = "2:00 PM";  // 4th time block
            const string TIME5 = "4:00 PM";  // 5th time block

            //Times array
            string[] times = new string[5];
            times[0] = TIME1;
            times[1] = TIME2;
            times[2] = TIME3;
            times[3] = TIME4;
            times[4] = TIME5;

            //Chars array
            char[] chars = new char[10];
            chars[0] = 'D';
            chars[1] = 'I';
            chars[2] = 'O';
            chars[3] = 'S';
            chars[4] = 'B';
            chars[5] = 'P';
            chars[6] = 'F';
            chars[7] = 'L';
            chars[8] = 'Q';
            chars[9] = 'V';

            const float SOPHOMORE = 30; // Hours needed to be sophomore
            const float JUNIOR = 60;    // Hours needed to be junior
            const float SENIOR = 90;    // Hours needed to be senior

            string lastNameStr;         // Entered last name
            char lastNameLetterCh;      // First letter of last name, as char
            string dateStr = "Error";   // Holds date of registration
            string timeStr = "Error";   // Holds time of registration
            float creditHours;          // Previously earned credit hours
            bool isUpperClass;          // Upperclass or not?

            lastNameStr = lastNameTxt.Text;
            if (lastNameStr.Length > 0) // Empty string?
            {
                lastNameLetterCh = lastNameStr[0];   // First char of last name
                lastNameLetterCh = char.ToUpper(lastNameLetterCh); // Ensure upper case

                if (float.TryParse(creditHoursTxt.Text, out creditHours) && creditHours >= 0)
                {
                    if (char.IsLetter(lastNameLetterCh)) // Is it a letter?
                    {
                        isUpperClass = (creditHours >= JUNIOR);

                        // Juniors and Seniors share same schedule but different days
                        if (isUpperClass)
                        {
                            if (creditHours >= SENIOR)
                                dateStr = DAY1;
                            else // Must be juniors
                                dateStr = DAY2;

                            if (lastNameLetterCh <= chars[0])      // A-D
                                timeStr = times[2];
                            else if (lastNameLetterCh <= chars[1]) // E-I
                                timeStr = times[3];
                            else if (lastNameLetterCh <= chars[2]) // J-O
                                timeStr = times[4];
                            else if (lastNameLetterCh <= chars[3]) // P-S
                                timeStr = times[0];
                            else                              // T-Z
                                timeStr = times[1];

                        }
                        // Sophomores and Freshmen
                        else // Must be soph/fresh
                        {
                            if (creditHours >= SOPHOMORE)
                            {
                                // A-B, P-Z on day one
                                if ((lastNameLetterCh <= chars[4]) ||  // <= B
                                    (lastNameLetterCh >= chars[5]))    // >= P
                                    dateStr = DAY3;
                                else // All other letters on next day
                                    dateStr = DAY4;
                            }
                            else // must be freshman
                            {
                                // A-B, P-Z on day one
                                if ((lastNameLetterCh <= chars[4]) ||  // <= B
                                    (lastNameLetterCh >= chars[5]))    // >= P
                                    dateStr = DAY5;
                                else // All other letters on next day
                                    dateStr = DAY6;
                            }

                            if (lastNameLetterCh <= chars[4])      // A-B
                                timeStr = times[4];
                            else if (lastNameLetterCh <= chars[0]) // C-D
                                timeStr = times[0];
                            else if (lastNameLetterCh <= chars[6]) // E-F
                                timeStr = times[1];
                            else if (lastNameLetterCh <= chars[1]) // G-I
                                timeStr = times[2];
                            else if (lastNameLetterCh <= chars[7]) // J-L
                                timeStr = times[3];
                            else if (lastNameLetterCh <= chars[2]) // M-O
                                timeStr = times[4];
                            else if (lastNameLetterCh <= chars[8]) // P-Q
                                timeStr = times[0];
                            else if (lastNameLetterCh <= chars[3]) // R-S
                                timeStr = times[1];
                            else if (lastNameLetterCh <= chars[9]) // T-V
                                timeStr = times[2];
                            else                              // W-Z
                                timeStr = times[3];
                        }

                        // Output results
                        dateTimeLbl.Text = $"{dateStr} at {timeStr}";
                    }
                    else // Not A-Z
                        MessageBox.Show("Make sure last name starts with a letter!");
                }
                else
                    MessageBox.Show("Enter a valid number of credit hours!");
            }
            else // Empty textbox
                MessageBox.Show("Please enter last name!");
        }
    }
}
